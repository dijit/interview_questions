Phone Interview:
================

Us:
---

 * our ideal candidate:
     1. Network Expert, Strong passion.
     2. Strong understanding of BGP/VPN
     3. Discipline. To find what to improve without being told to.
     4. Willing to learn other disciplines
     5. Moderate ability with Scripting (Powershell/Python)

Them:
-----

 * What are they looking for:
     * Relocation to Sweden
     * Team:
         * 2 Senior engineers
         * Closely working with Online Programmers (C++).
         * Highly scalable physical infrastructure. (Homegrown)
         * Growing team (1 infra eng position to fill)
         * Interesting scalability challenges.
     * Company:
         * Growing since release of TCTD.
         * 2 Projects incoming, now is the best time to join in order to impact
         * Strong team spirit.

Our Tech:
---------

 * SaltStack
 * Cisco
 * Brocade
 * IPSec VPN
 * OpenVPN
 * FreeBSD
 * Windows 2012 R2 + AD
 * Python/Powershell
 * Monitoring: Zabbix, Graphite/StatsD, Logstash/Kibana/ES

Standard:
---------

 * Can you tell me a little about what you did for $employer[-1]
 * What was the hardest challenge you overcame in your career?
    * How were you instrumental to remediating the situation?
 * How do you come across new things in tech?
 * How do you manage your day-to-day work?
 * What is your preferred development environment?
 * What is the largest environment you've ever worked with, and for how long.
    * Network Throughput/PPS.
    * Memory Consumption.
    * CPU Cores.
    * Number of machines.

Finally:
 * What makes you feel valued/rewarded?
