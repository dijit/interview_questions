#!/usr/bin/env bash
pandoc --from markdown_github --to html --standalone README.md | sed 's:<meta name="generator" content="pandoc" />:<link rel="stylesheet" type="text/css" href="basic.css">:' > README.html
