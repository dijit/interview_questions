Storage Test
============

Please design a storage system backend which can be used for storing large,
random, binary data (1MB files, 1 per active user).

An ideal solution would be fully owned and controlled by Ubisoft Massive.

The interface can be anything that can be used across multiple servers,
(I.E. Database, filesystem mount, HTTP).

Assume effective concurrency of 1 million players. Assume storage space for
10 million additional inactive players.

Players must be able to play on *any* gameserver.

Please ensure that any system designed has appropriate conditions for failure,
we are aiming for CA (Consistency, Availability).

Assume no prior knowledge of storage technologies by the audience.

Please be prepared to explain how the solution will work fundamentally to a
sceptical audience.

This is open ended, make all assumptions you need but be prepared to be
explicit about your assumptions.
e.g., unlimited budget, gameservers will retry indefinitely, etc;

Constraints:

* Gamservers are geographically distributed as to be close to the player,
    do not worry about internet links being saturated.
* Consistency must be achieved to a high degree.
    All acknowledgements must ensure that data is persistent.
* Writes are typically 1MB in size but may vary.
* Stock storage throughput must be 1,750* writes per second or higher.
* Burst throughput must be able to handle 4,000 writes per second.
* No write may take longer than 60s
