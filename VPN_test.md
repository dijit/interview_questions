Network engineer test.
======================

Welcome to $bigcorp!

Unfortunately, last week our senior networking architect took his parental leave
He is currently off the coast of Africa with his newborn baby daughter and no
mobile phone reception. This would not be an issue if our senior network engineer
had not been hit by a bus. She will be missed. :(

What does this mean? Well, now we lost all our plans for our new game which
requires global networking! :(

Luckily we still have the requirements in our email outbox so we can forward
them to you!

--------
Subject: FW: New Game! Need stuff.

Hi Network team!

We're launching a new game and we're going with an all-new provider.

Luckily this means we can get rid of all the old networking hardware and
paradigms, HQ says: "As long as it works; it's fine with us" so we have
basically unlimited budget, although we'll have to justify it of course.

What we need is a global VPN to connect our gameserver datacenters with our
main datacenter where user profiles will be stored and matchmaking will occur:

We're going to have game server datacenters in:

 - Los Angeles, USA
 - New York City, USA
 - Amsterdam, NL
 - London, UK
 - Hong Kong, CN
 - Sydney, AU

And our central master coordination/save datacenter is in Paris, FR.

We're not so worried about the security of it, almost any cipher is fine..
performance is important, our game developers are expecting 10,000,000 Packets
per second and up to 100GBit of throughput towards the master datacenter.

It also needs to be able to self-heal, Sarah the network engineer on your team
said something about how you can do some magic with a thing called "BGP".
I have no idea what it means but as long as it fails over and comes back online
then I want it!

Your report should contain, but not be limited to: What products can we use?
How much will it cost? How can we have redundancy? Can we do active-active?
What are the considerations of doing so? and so on.

Please prepare to present this report in person to the Directors.

Please assume no technical knowledge on behalf of the attendees, they do not
like feeling stupid.

Signed,
    Mr. Bigwig.
    Super Expert Senior Tech Director.
    $bigcorp
