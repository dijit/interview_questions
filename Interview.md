Phone Interview:
================

Us:
---

 * our ideal candidate:
     1. Python Expert
     2. Tolerant to the idea of a Windows platform.
     3. Discipline. To find what to improve without being told to.
     4. Willing to learn other disciplines. (networking/ops)

Them:
-----

 * What are they looking for:
     * Relocation to Sweden
     * Team:
         * 3 Senior engineers + 1 Manager + 1 Netadmin
         * Closely working with Online Programmers (C++,Windows).
         * Highly scalable physical infrastructure. (Homegrown)
         * Constantly Learning.
         * Interesting scalability challenges. (Consistency:Throughput of DBS)
     * Company:
         * Growing since release of TCTD.
         * 2 Projects incoming, now is the best time to join in order to impact
         * Strong team spirit.

Our Tech:
---------

 * SaltStack
 * CentOS (deprecated)
 * FreeBSD
 * Windows 2012 R2 (deprecated)
 * Windows Server 2016
 * Python/Powershell
 * Monitoring: Zabbix, telegraf/influxdb, Logstash/Kibana/ES

Standard:
---------

 * Can you tell me a little about what you did for $employer[-1]
 * What was the hardest challenge you overcame in your career?
    * How were you instrumental to remediating the situation?
 * How do you come across new things in tech?
 * How do you manage your day-to-day work?
 * What is your preferred development environment?
 * What is the largest environment you've ever worked with, and for how long.
    * Network Throughput/PPS.
    * Memory Consumption.
    * CPU Cores.
    * Number of machines.
 * Describe, in as much detail as you're comfortable: how HTTP works.
 * I want to put 50TB in a database, is that possible? what are the drawbacks?

Finally:
 * What makes you feel valued/rewarded?
